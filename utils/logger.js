var winston = require('winston');
var Log = require("express-winston-middleware").Log;

winston.emitErrs = true;
var log = new Log({
    transports: [
        new (winston.transports.Console)({ json: true })
    ]
}, {
    // Metadata to add to each log response.
    foo: "bar"
});