'use stick';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * @module publication
 * @description contain the details of publication information, conditions and actions.
 */
var candidatesModel = new Schema({
    candidateId:String,
    candidateName: String,
    party_full: String,
    state: String,
    fullCandidate: String,

});

var candidatesListMongo = mongoose.model('candidatesList', candidatesModel);

/**
 * Save function to create publication object in the database.
 * @param publicationData the publication object to be stored in the database.
 * @param callback returns the result and error from the database.
 */
exports.save = function (candidatesMongoData, callback) {
    var newcandidatesMongoData = new candidatesListMongo(candidatesMongoData);
    newcandidatesMongoData.save(callback);
};

/**
 * FindAll function to find all the publication objects in the database.
 * @param callback returns the result and error from the database.
 */
exports.findAll = function (callback) {
    candidatesListMongo.find(callback);
};

/**
 * FindsOne function to find the publication object from the database based on id.
 * @param id the id to be searched.
 * @param callback returns the result and error from the database.
 */
exports.findOne = function (id, callback) {
    candidatesListMongo.findById(id, callback);
};
/*custom find*/
exports.find = function (input, callback) {
    candidatesListMongo.find(input, callback);
};

/**
 * Update function to update the publication object in the database based on id.
 * @param id the id to be updated
 * @param publicationData the publication object to be updated in the database.
 * @param callback returns the result and error from the database.
 */
exports.update = function (id, nytMongoData, callback) {
    candidatesListMongo.findByIdAndUpdate(id, nytMongoData, callback);
};

/**
 * Delete function to delete the publication object from the database based on id.
 * @param id the id to be deleted
 * @param publicationData the publication object to be deleted from the database.
 * @param callback returns the result and error from the database.
 */
exports.remove = function (id, nytMongoData, callback) {
    candidatesListMongo.findByIdAndRemove(id, nytMongoData, callback);
};
