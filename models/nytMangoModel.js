'use stick';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * @module publication
 * @description contain the details of publication information, conditions and actions.
 */
var nytMongoModel = new Schema({
    id:String,
    section: String,
    title: String,
    abstract: String,
    url: String,
    byline: String,
    thumbnail_standard: String,
    source: String,
    updated_date: String

});

var nytMongo = mongoose.model('nytNews', nytMongoModel);

/**
 * Save function to create publication object in the database.
 * @param publicationData the publication object to be stored in the database.
 * @param callback returns the result and error from the database.
 */
exports.save = function (nytMongoData, callback) {
    var newNytMongo = new nytMongo(nytMongoData);
    newNytMongo.save(callback);
};

/**
 * FindAll function to find all the publication objects in the database.
 * @param callback returns the result and error from the database.
 */
exports.findAll = function (callback) {
    nytMongo.find(callback);
};

/**
 * FindsOne function to find the publication object from the database based on id.
 * @param id the id to be searched.
 * @param callback returns the result and error from the database.
 */
exports.findOne = function (id, callback) {
    nytMongo.findById(id, callback);
};

/**
 * Update function to update the publication object in the database based on id.
 * @param id the id to be updated
 * @param publicationData the publication object to be updated in the database.
 * @param callback returns the result and error from the database.
 */
exports.update = function (id, nytMongoData, callback) {
    nytMongo.findByIdAndUpdate(id, nytMongoData, callback);
};

/**
 * Delete function to delete the publication object from the database based on id.
 * @param id the id to be deleted
 * @param publicationData the publication object to be deleted from the database.
 * @param callback returns the result and error from the database.
 */
exports.remove = function (id, nytMongoData, callback) {
    nytMongo.findByIdAndRemove(id, nytMongoData, callback);
};
