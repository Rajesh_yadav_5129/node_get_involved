var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
//var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var parser = require('body-parser-xml')(bodyParser);
var winston = require('winston');
var MongoClient = require('mongodb').MongoClient;
const fileUpload = require('express-fileupload');
var cors = require('cors')
var url = "mongodb://localhost:27017/getInvolved";
var db = require('./config/db');


var fs = require('fs');

var app = express();
var logger = new winston.Logger({
        transports: [
            new winston.transports.File({
                name :             'info',
                level:            'info',
                filename:         './logs/all-logs.log',
                handleExceptions: true,
                json:             true,
                maxsize:          5242880, //5MB
                maxFiles:         50,
                colorize:         true
            }),
            new winston.transports.Console({
                level:            'debug',
                handleExceptions:  true,
                json:              false,
                colorize:          true
            }),
            new winston.transports.File({
                name : 'error',
                level:            'error',
                filename:         './logs/error.log',
                handleExceptions: true,
                json:             true,
                maxsize:          5242880, //5MB
                maxFiles:         50,
                colorize:         true
            })
        ],
        exitOnError: false
    });

    logger.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};

app.use(require("morgan")("combined", { "stream": logger.stream }));


var router = require('./router');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cors());

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
//app.use(bodyParser.json());
app.use(bodyParser.json({limit: '5mb'}));
//app.use(bodyParser.raw());
app.use(bodyParser.xml({ type: 'text/xml' }));
app.use(fileUpload());
// for parsing multipart/form-data

//app.use(express.json());
//app.use(express.urlencoded());
//app.use(xmlparser({ type: 'application/p' }));
//app.use(xmlparser());
//app.use(express.urlencoded());
//app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '5mb',  extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', router);


      app.all('/*', function (request, response, next) {
        response.header("Access-Control-Allow-Origin", "*");
        response.header("Access-Control-Allow-Headers", "X-Requested-With");
        response.header("Access-Control-Allow-Methods", "GET, POST", "PUT", "DELETE");
        next();
    });


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
