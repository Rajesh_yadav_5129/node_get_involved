
exports.getAllPostsService = function (request,response,client) {
    client.getPosts(function( error, posts ) {
        if (error) throw error;
        response.send(posts)
    });
};

exports.createPost = function (request,response,client) {

    client.newPost({
        title: request.body.title,
        content: request.body.content,
        status: "publish"
    }, function( error, data ) {
        console.log( arguments );
        response.send(arguments)
    });

};

exports.editPost = function (request,response,client) {
client.editPost(request.body.postId,
    request.body.data
    ,function (error) {
        console.log(arguments)
        response.send(arguments)
    })
};

exports.getPost = function (request,response,client) {
client.getPost(request.body.postId,function (error,post) {
    response.send(post);
})
};
exports.deletePost = function (request,response,client) {
    client.deletePost(request.body.postId,function (error) {
        response.send(arguments);
    });
};
exports.attachFileToPost = function (request,response,client)
{
    var filename = "get-involed-logo.jpg";
    var file = fs.readFileSync( filename );
    client.uploadFile({
        name: filename,
        type: "image/jpg",
        bits: file
    }, function( error, data ) {
        console.log( arguments );
    });
};