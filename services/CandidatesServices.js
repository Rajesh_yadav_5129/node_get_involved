var restClient = require('../restclients/commonRestClient');
var candidatesModel = require('../models/candidatesModel');
var candidatesAPIKey = 'NkFvI6Egux7RETLbJkWdW5znOgDE0Xy7dCKRvLg7';
//'https://api.open.fec.gov/v1/candidate/'+request.body.candidateId+'/?sort=name&page='+request.body.pageNumber+'&per_page='+request.body.numberOfRecords+'&api_key='+candidatesAPIKey
exports.getAllCandidates = function (request,response)
{
this.getCandidates(request.body.pageNumber,request.body.numberOfRecords,function (data) {
response.send(data)
});
};
exports.getCandidate = function(request,response)
{
    var getCandidateById = 'https://api.open.fec.gov/v1/candidate/'+request.body.candidateId+'/?sort=name&page='+request.body.pageNumber+'&per_page='+request.body.numberOfRecords+'&api_key='+candidatesAPIKey;
    restClient.getServiceClient(getCandidateById ,function (data) {
        response.send(data);
    })
};
exports.getCandidates = function(pageNumber,numberOfRecords,callback){

    var getAllCandidatesService = 'https://api.open.fec.gov/v1/candidates/search/?sort=name&page='+pageNumber+'&per_page='+numberOfRecords+'&api_key='+candidatesAPIKey;
    restClient.getServiceClient(getAllCandidatesService,function (data) {
        callback(data)
    });

};

/*save candidates data from trigger */
exports.saveCandidateByTrigger = function (data, callback) {
    candidatesModel.save(data, function (error, data) {
        if(!error){
            callback(data);
        }else{
            callback(error);
        }
    });
};

exports.findUser = function (data,callback) {
    candidatesModel.find(data,function (err,user) {
        if(err) throw err;
        callback(user)
    });
};

exports.searchCandidate = function (req,res) {
    var candidateName =req.body.candidateName;
    candidatesModel.find({ "candidateName" : { $regex: candidateName.substring(1, candidateName.length-1), $options: 'i' } },
        function (err, candidates) {
            if (err) return handleError(err);
            res.send(candidates) ;
        });
};

