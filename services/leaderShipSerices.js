var errorModel = require('../Errors/clientErrorCodes');
var HTTPStatus = require('http-status');
exports.getAddressService = function (req, res,connection)
{
    var emailId = req.body.emailId;
    var password = req.body.password;
    var sql = "select * from gi_address";
    connection.query(sql, function (err, result, fields) {
        if (err) throw err;
        var response;

        if(result.length>0) {
            response = {
                "status":"success",
                "result":result
            }
            res.send(JSON.stringify(response));

        }
        else {

            response = {
                "status":"no data",
                "result":[]
            }
            res.send(JSON.stringify(response));

        }

    })
}

exports.getLeadersList = function(req, res,mysqlConnections){

    var sql = "Select sl.*,slt.* from state_leader as sl left join state_leader_types as slt on sl.type=slt.id";
    mysqlConnections.query(sql, function (err, result) {
        if (err) throw err;
        if(result === undefined){
            var errorResponse = errorModel.clientErrorCodesModel(
                HTTPStatus[res.statusCode],
                res.statusCode,
                res.statusCode
            );
            res.send(JSON.stringify(errorResponse));
        }else{
            res.send(JSON.stringify(result));
        }
    });

};

