var nytMangoModel = require('../models/nytMangoModel');

/**
 * Create function to create a publication object.
 */
exports.create = function (request, response) {
    nytMangoModel.save(request.body, function (error, publicationData) {
        if(!error){
            response.json(publicationData);
        }else{
            response.send(error);
        }
    });
};
/*inserting data from trigger */
exports.createByTrigger = function (data, callback) {
    nytMangoModel.save(data, function (error, data) {
        if(!error){
            callback(data);
        }else{
            callback(error);
        }
    });
};


/**
 * GetAll function to get all the publication objects.
 */
exports.getAll = function (request, response) {
    nytMangoModel.findAll(function (error, publicationsData) {
        if(!error){
            response.json(publicationsData);
        }else{
            response.send(error);
        }
    });
};

/**
 * Get function to get a publication object by id.
 */
exports.get = function (request, response) {
    nytMangoModel.findOne(request.params.id, function (error, publicationData) {
        if(!error){
            response.json(publicationData);
        }else{
            response.send(error);
        }
    });
};

/**
 * Update function to update a publication object based on id.
 */
exports.update = function (request, response) {
    nytMangoModel.update(request.body._id, request.body, function (error, publicationData) {
        if(!error){
            response.json(publicationData);
        }else{
            response.send(error);
        }
    });
};

/**
 * Delete function to delete a publication object based on id.
 */
exports.delete = function (request, response) {
    nytMangoModel.remove(request.body._id, request.body, function (error, publicationData) {
        if(!error){
            response.json(publicationData);
        }else{
            response.send(error)
        }
    });
};