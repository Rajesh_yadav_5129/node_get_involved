var express = require('express');
var router = express.Router();
var multer = require('multer');
var wordpress = require("wordpress");
var databaseConnection = require('./utils/DataBaseConnections');
con = databaseConnection.mysqlConnection();
var UserManagmentServices = require('./services/UserManagmentServices');
var GIProfileManagment = require('./services/GIProfileManagment');
var AggregationServices = require('./services/AggregationServices');
var AffiliationServices = require('./services/AffiliationServices');
var RepublicanServices = require('./services/republicService');
var DemocraticService = require('./services/democraticService');
var RepublicdemocreticService = require('./services/republicdemocreticService');
var LeaderShipSerices = require('./services/leaderShipSerices');
var KeyWordsService = require('./services/KeyWordsService');

var cnnService = require('./services/cnnService');
var nbcService = require('./services/nbcService');
var nytService = require('./services/nytService');
var nytMongoService = require('./services/nytMongoService');
var blogServices = require('./services/BlogServices');
var candidatesServices = require('./services/CandidatesServices');
/*wordpress connection*/

var wpClient = wordpress.createClient({
    url: "http://localhost/wordpress_Node/",
    username: "rajesh",
    password: "ipro@2016"
});

/*polecy service*/
router.post('/gentInvolved/service/Affiliation/addAffiliation', function (req, res) {

    var giProfile = req.body;
    console.log(giProfile);
    //console.log(req.files.file.name);
    var privacy_policy_file = req.files.file;
    var fileType = req.files.file.mimetype;
    var finalFilePath = './public/uploads/' + req.files.file.name;
    var sqlFilePath = './uploads/' + req.files.file.name;
    privacy_policy_file.mv(finalFilePath, function (err) {
        if (err)
            return res.status(500).send(err);
        res.send("ok");
        /*var sql = "insert into gi_affiliation(name,logo,url,description) " +
            "values('" + giProfile.name + "','" + sqlFilePath + "','" + giProfile.url + "','" + giProfile.description + "')";
        con.query(sql, function (err, result, fields) {
            if (err) throw err;
            res.send(result);

        })*/

    });


});

router.post('/gentInvolved/service/Affiliation/updateAffiliation', function (req, res) {
    try {
        var giProfile = req.body;
//console.log(req.files.file.name);
        if (req.body.status == "no") {
            var sql = "UPDATE gi_affiliation SET name='" + giProfile.name + "',url='" + giProfile.url + "',description='" + giProfile.description + "' WHERE id=" + giProfile.id + "";

            con.query(sql, function (err, result, fields) {
                if (err) throw err;
                res.send(result);

            })

        }
        else {
            var privacy_policy_file = req.files.file;
            var finalFilePath = './public/uploads/' + req.files.file.name;
            var sqlFilePath = './uploads/' + req.files.file.name;
            privacy_policy_file.mv(finalFilePath, function (err) {
                if (err)
                    return res.status(500).send(err);
                var sql = "UPDATE gi_affiliation SET logo='" + sqlFilePath + "', name='" + giProfile.name + "',url='" + giProfile.url + "',description='" + giProfile.description + "' WHERE id=" + giProfile.id + "";

                con.query(sql, function (err, result, fields) {
                    if (err) throw err;
                    res.send(result);

                })

            });

        }
    }
    catch (err) {
        console.log(err.message);
    }

});


/*login service*/

router.post('/gentInvolved/service/UserManagmentServices/login', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
    UserManagmentServices.userLogin(req, res, con);
});

router.get('/gentInvolved/service/cnnNews', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    cnnService.getCNNData(req, res);
});

router.get('/gentInvolved/service/nbcNews', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    nbcService.getNbcNews(req, res);
});

router.get('/gentInvolved/service/nytNews', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    nytService.getNytNews(req, res);

});


router.post('/gentInvolved/service/GIProfileManagment/updateGIProfiledata', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    GIProfileManagment.updateGIProfiledata(req, res, con);

});

router.get("/gentInvolved/service/GIProfileManagment/getGIProfileDetails", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    GIProfileManagment.getGIProfileDetails(req, res, con);

});
router.get("/gentInvolved/service/UserManagmentServices/userDetails", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    UserManagmentServices.getUserDetails(req, res, con);

});
router.post("/gentInvolved/service/UserManagmentServices/createUser", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    UserManagmentServices.createUser(req, res, con);

});
router.post("/gentInvolved/service/UserManagmentServices/updateUserStatus", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    UserManagmentServices.userStatus(req, res, con);

});
router.get("/gentInvolved/service/AggregationServices/getAggregationData", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    AggregationServices.getAggrigationData(req, res, con);

});
router.get("/gentInvolved/service/AffiliationServices/getAffiliation", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    AffiliationServices.getAffiliations(req, res, con);

});

router.post("/gentInvolved/service/AffiliationServices/getSubAffiliation", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    AffiliationServices.getSubAffiliations(req, res, con);

});
router.get("/gentInvolved/service/AffiliationServices/getSubAffiliationDetails", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    AffiliationServices.getSubAffiliationsDetails(req, res, con);

});
router.get("/gentInvolved/service/NewsData/republicanNews", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    RepublicanServices.getRepublicnews(req, res);

});
router.get("/gentInvolved/service/NewsData/democretNews", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    DemocraticService.getDemocretnews(req, res);

});
router.get("/gentInvolved/service/NewsData/bothNews", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    RepublicdemocreticService.getBothNews(req, res);

});
router.get("/gentInvolved/service/leaderShipServices/getAddress", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    LeaderShipSerices.getAddressService(req, res, con);

});
router.get("/gentInvolved/service/leaderShipServices/getLeadersList", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    LeaderShipSerices.getLeadersList(req, res, con);

});
router.get("/gentInvolved/service/KeyWordsService/getKeywords", function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    KeyWordsService.generateKeywords(req, res);

});
router.post('/gentInvolved/service/saveToMango', function (request, response) {
    nytMongoService.create(request, response);
});

router.get('/gentInvolved/service/getAllNews', function (request, response) {
    nytMongoService.getAll(request, response);
});

/*blog services start*/

router.get('/gentInvolved/service/blogServices/getAllPosts', function (request, response) {
    blogServices.getAllPostsService(request, response, wpClient);
});
router.post('/gentInvolved/service/blogServices/addPost', function (request, response) {
    blogServices.createPost(request, response, wpClient);
});
router.post('/gentInvolved/service/blogServices/editPost', function (request, response) {
    blogServices.editPost(request, response, wpClient);
});
router.post('/gentInvolved/service/blogServices/getPost', function (request, response) {
    blogServices.getPost(request, response, wpClient);
});
router.post('/gentInvolved/service/blogServices/deletePost', function (request, response) {
    blogServices.deletePost(request, response, wpClient);
});

/*blog services end*/

/* api.open.fec.gov API services start*/

router.post('/gentInvolved/service/candidatesServices/getAllCandidates', function (request, response) {
    candidatesServices.getAllCandidates(request, response);
});
router.post('/gentInvolved/service/candidatesServices/getCandidateById', function (request, response) {
    candidatesServices.getCandidate(request, response);
});
router.post('/gentInvolved/service/candidatesServices/searchCandidate', function (request, response) {
    candidatesServices.searchCandidate(request, response);
});

/*api.open.fec.gov API services end*/


/* trigger for nyt service */
var trigger = function () {
    nytService.getNytApiNews(function (response) {
        var parsedResponse = JSON.parse(response).results;
        for (var i = 0; i <= parsedResponse.length - 1; i++) {
            /*&& parsedResponse[i].subsection=="Politics"*/
            if (parsedResponse[i].section == "U.S." && parsedResponse[i].item_type != "Video" && parsedResponse[i].subsection == "Politics") {
                var parsedNewsItem = parsedResponse[i];
                KeyWordsService.generateKeywords(parsedResponse[i].url, function (keyWords) {
                var newsItem = {
                        "abstract": parsedNewsItem.abstract,
                        "byline": parsedNewsItem.byline,
                        "id": parsedNewsItem.id,
                        "section": parsedNewsItem.section,
                        "source": parsedNewsItem.source,
                        "thumbnail_standard": parsedNewsItem.thumbnail_standard,
                        "title": parsedNewsItem.title,
                        "updated_date": parsedNewsItem.updated_date,
                        "url": parsedNewsItem.url,
                        "Keywords": keyWords
                    };
                    nytMongoService.createByTrigger(newsItem, function (data) {
                        console.log(data)
                    })
                })
            }
            else {
            }
        }


    });

};
//setInterval(trigger,3600000);

/*candidate details trigger*/
var numberOfPages=0;
var currentPage=1;
var numberOfRecordsPerRequest=100;
var serviceRequestCount=0;
var candidatesTrigger = function ()
{
    var callService =function () {
        console.log("in side trigger"+serviceRequestCount);
        candidatesServices.getCandidates( currentPage, numberOfRecordsPerRequest, function (data) {
            var candidatesLength = data.results.length;
            numberOfPages = data.pagination;
            var count = 0;
            console.log(data)
            /*check candidate exist or not*/
            saveUser(count);
            function saveUser(count) {
                var candidateObject = data.results[count];
                if (candidateObject != undefined) {
                    var newCandidate = {
                        "candidateId": candidateObject.candidate_id,
                        "candidateName": candidateObject.name,
                        "party_full": candidateObject.party_full,
                        "state": candidateObject.state,
                        "fullCandidate": JSON.stringify(candidateObject)
                    };
                    candidatesServices.findUser({"candidateId": candidateObject.candidate_id}, function (data) {
                        count++;
                      //  console.log(count+'--'+candidatesLength);
                        if (count <= (parseInt(candidatesLength))) {
                            if (data.length == 0) {
                                candidatesServices.saveCandidateByTrigger(newCandidate, function (data) {
                                    saveUser(count);
                                });
                            }
                            else {
                                console.log('parsent'+count);
                                saveUser(count);
                            }
                            if(count == (parseInt(candidatesLength)))
                            {
                                currentPage++;
                                serviceRequestCount++;
                                if(parseInt(numberOfPages)==parseInt(serviceRequestCount)) {

                                }
                                else {
                                    callService();
                                }
                            }
                        }
                    });
                }
            }
        });
    };
    callService();
};
/*candidatesServices.getCandidates(1, 100, function (data) {
    console.log(data.pagination.pages)
});*/
//setInterval(candidatesTrigger, 60000);
//candidatesTrigger();
module.exports = router;
