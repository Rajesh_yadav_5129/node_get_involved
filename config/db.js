var Mongoose = require('mongoose');

Mongoose.connect('mongodb://localhost/getInvolved');
var db = Mongoose.connection;
db.on('error', console.error.bind(console, 'Database Connection Error.'));
db.once('open', function callback() {
    console.log("Database Connected!!!");
});
exports.db = db;