exports.clientErrorCodesModel = function(message,statusCode,errorCode) {
    var errorObject = {
        "Error": {
            "Message": {
                     "message": message,
                    "errorCode": errorCode,
                    "statusCode": statusCode
                }
            }
    };
    return errorObject;
};